/// <reference path="../../../wwwroot/js/typings/moment/moment.d.ts" />

'use strict';

module Comercial.BotonMenu {

    export class BotonMenu extends Common.Controller<any>{
        template: string;
        props: any;
        methods: any;

        constructor(params) {
            super();
            this.template = "#boton-menu-component";
            this.props = {
                titulo: { type: String, default: ''},
                subtitulo_linea1: { type: String, default: ''},
                subtitulo_linea2: { type: String, default: ''},
                subtitulo_linea3: { type: String, default: ''},
                link: { type: String, default: '#'},
                icono: { type: String, default: ''},
                onclick: { type: String, default: ''},
                class: {type: String, default: 'col-md-6 col-sm-12'}
            };

            this.methods = {
                cambiar: function (onclick) {
                    onclick()
                    // this.$nextTick(() => {
                    //     this.$parent.$emit(onclick);
                    // });
                }
            }
        }
    }

}