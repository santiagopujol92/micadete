import firebase from 'firebase';
import Constants from 'expo-constants';
    
// Inicializamos firebase si no esta iniciado
if (firebase != null) {
    if (firebase.apps != null) {
        firebase.apps.length !== 0 || firebase.initializeApp(Constants.manifest.extra.firebase);
    }
}

var database = firebase.database();

export default database;

// HACER AUDITORIA CON DE TODAS LAS OPERACIONES