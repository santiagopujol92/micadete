import React, { useState, useEffect  } from 'react';

import {
    Box,
    useColorModeValue,
    useColorMode,
    useDisclose
    } 
from 'native-base';

import { LoginView } from '../views/LoginView';
import { HeaderView } from './HeaderView';
import { MainBodyView } from './MainBodyView';
import { FooterComponent } from '../components/FooterComponent';

import { LoadingModalComponent } from '../components/modals/LoadingModalComponent';
import { TypeUserSelectionModalComponent } from '../components/modals/TypeUserSelectionModalComponent';

import * as Location from 'expo-location';
import database from '../services/firebase';

console.disableYellowBox = true; // TEST - ELIMINAMOS ALERTAS DE LA APP, LUEGO LAS VAMOS A SOLUCIONAR

export function IndexView() {

    // Hook Estados (pasarlos a archivo customHooks usar Context)
    const [isLoggedin, setLoggedinStatus] = useState(false);
    const [userData, setUserData] = useState(null);
    const [isProfileImageLoading, setProfileImageLoadStatus] = useState(false);
    const {colorMode, toggleColorMode} = useColorMode();

    const [modalOpenLoading, setModalOpenLoading] = useState(false);
    const [modalOpenSelectionTypeUser, setModalOpenSelectionTypeUser] = useState(false);

    // const [openABMPedidoView, setOpenABMPedidoView] = useState(true);

    const [modalOpenDireccionForm, setModalOpenDireccionForm] = useState(false);
    const [modalOpenEncomiendaForm, setModalOpenEncomiendaForm] = useState(false);

    const [dataFormDomicilio, setDataFormDomicilio] = useState({});
    const [dataFormDomicilioDestino, setDataFormDomicilioDestino] = useState({});
    const [dataFormEncomienda, setDataFormEncomienda] = useState({});
    const [dataSelectCadete, setDataSelectCadete] = useState([]);
    const [dataSelectCadeteSelected, setDataSelectCadeteSelected] = useState({});
    const [misPedidosData, setMisPedidosData] = useState([]);

    const [selectedTabButtonFooter, setSelectedTabButtonFooter] = useState("Mis Pedidos");
    const {isOpen, onOpen, onClose} = useDisclose();

    const [abmPedidoData, setABMPedidoData] = useState(null); 
    const [pantallaAnteriorABMPedido, setPantallaAnteriorABMPedido] = useState(null); 

    const [time, setTime] = useState({});
    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
    const [city, setCity] = useState(null);

    // Se crea Box con configuracion de cambio de tema
    const BoxWithActualTheme = ({ children }) => {
        const bg = useColorModeValue('gray.200', 'gray.800')
        return (
            <Box bg={bg} flex={1}>
                {children}
            </Box>
        );
    };

    const getLocationAndCity = async () => {
        let {status} = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
            setErrorMsg('Access to Location denied');
        }

        const location = await Location.getCurrentPositionAsync({});
        setLocation(location)

        const place = await Location.reverseGeocodeAsync({
            latitude : location.coords.latitude,
            longitude : location.coords.longitude
        });

        setCity(place)
        const response = await TimeApi.get(`/${place}.json`);
        setTime(response.data);
    }

    // Obtiene pedidos y filtra por clave igual al usuario local segun sea cadete o cliente y tenga seteado el userCadete
    const getDataMisPedidos = async () => {
        const dataRef = database.ref("pedidos");
        let list = [];

        // Obtener data al iniciar el componente
        dataRef.on('value', (snapshot) =>{
            list = [];
            console.log("hola")
            snapshot.forEach((child)=>{
                var dataPedido = {
                    key: child.key,
                    id: child.key,
                    userCliente: child.val().userCliente,
                    userCadete: child.val().userCadete,
                    fechaAlta: child.val().fechaAlta,
                    fechaUltimaModif: child.val().fechaUltimaModif,
                    estado: child.val().estado,
                    direccionOrigen: child.val().direccionOrigen,
                    direccionDestino: child.val().direccionDestino,
                    encomienda: child.val().encomienda,
                    costoEnvio: child.val().costoEnvio,
                    mensajes: child.val().mensajes
                };

                if (userData != null) {
                    switch (userData.typeUser) {
                        case ("Cliente"):
                            // Si el pedido tiene seteado un cliente y es igual a la del logeado
                            if (child.val().userCliente != null && child.val().userCliente.id === userData.id) {
                                // console.log(child.val())

                                list.push(dataPedido);
                            }
                        case ("Cadete"):
                            // Si el pedido tiene seteado un cadete y es igual a la del logeado
                            if (child.val().userCadete != null && child.val().userCadete.id === userData.id) {
                                list.push(dataPedido);
                                console.log("hola3")

                            }

                        default: break;
                    }
                }else {
                    list.push(dataPedido);
                }

            })
            setMisPedidosData(list);
        });
    }

    // Filtra pedidos que no tengan seteado el userCadete
    const getDataPedidosPublicos = async () => {
        // const dataRef = database.ref("pedidos");
        // dataRef.on('value', (snapshot) =>{
        //     let li = [];
        //     snapshot.forEach((child)=>{
        //         li.push({
        //             key: child.key,
        //             id: child.key,
        //             userCliente:child.val().userCliente,
        //             userCadete: child.val().userCadete,
        //             fechaAlta: child.val().fechaAlta,
        //             fechaUltimaModif: child.val().fechaUltimaModif,
        //             estado: child.val().estado,
        //             direccionOrigen: child.val().direccionOrigen,
        //             direccionDestino: child.val().direccionDestino,
        //             encomienda: child.val().encomienda,
        //             costoEnvio: child.val().costoEnvio,
        //             mensajes: child.val().mensajes
        //         })
        //     })
        //     setMisPedidosData(li);
        // })
    }

    // On Start App
    useEffect(() => {
        // getDataPedidosPublicos();
        getDataMisPedidos();
        // getLocationAndCity();
    } , []);

    return (
        (isLoggedin) ? 
            (userData) ?
            <BoxWithActualTheme>
                    <HeaderView
                        userDataState={{
                            userData: userData, 
                            setUserData: setUserData
                        }}                         
                        isProfileImageLoadingState={{
                            isProfileImageLoading: isProfileImageLoading, 
                            setProfileImageLoadStatus: setProfileImageLoadStatus
                        }}
                        colorModeState={{
                            colorMode: colorMode,
                            toggleColorMode: toggleColorMode
                        }}
                        isLoggedinState={{
                            isLoggedin:isLoggedin,
                            setLoggedinStatus:setLoggedinStatus
                        }}
                        actionSheetState={{
                            isOpen: isOpen, 
                            onOpen: onOpen,
                            onClose: onClose
                        }}
                        modalOpenLoadingState={{
                            modalOpenLoading: modalOpenLoading, 
                            setModalOpenLoading: setModalOpenLoading
                        }}
                        tabButtonFooterState={{
                            selectedTabButtonFooter: selectedTabButtonFooter, 
                            setSelectedTabButtonFooter: setSelectedTabButtonFooter
                        }}
                        abmPedidoDataState={{
                            abmPedidoData: abmPedidoData,
                            setABMPedidoData: setABMPedidoData
                        }}
                        pantallaAnteriorABMPedidoState={{
                            pantallaAnteriorABMPedido: pantallaAnteriorABMPedido,
                            setPantallaAnteriorABMPedido: setPantallaAnteriorABMPedido
                        }}
                        modalOpenDireccionFormState={{
                            modalOpenDireccionForm: modalOpenDireccionForm,
                            setModalOpenDireccionForm: setModalOpenDireccionForm
                        }}
                        modalOpenEncomiendaFormState={{
                            modalOpenEncomiendaForm: modalOpenEncomiendaForm,
                            setModalOpenEncomiendaForm: setModalOpenEncomiendaForm
                        }}
                    />
                    <MainBodyView 
                        userDataState={{
                            userData: userData, 
                            setUserData: setUserData
                        }}                         
                        colorModeState={{
                            colorMode: colorMode,
                            toggleColorMode: toggleColorMode
                        }}
                        tabButtonFooterState={{
                            selectedTabButtonFooter: selectedTabButtonFooter, 
                            setSelectedTabButtonFooter: setSelectedTabButtonFooter
                        }}
                        modalOpenLoadingState={{
                            modalOpenLoading: modalOpenLoading, 
                            setModalOpenLoading: setModalOpenLoading
                        }}
                        abmPedidoDataState={{
                            abmPedidoData: abmPedidoData,
                            setABMPedidoData: setABMPedidoData
                        }}
                        pantallaAnteriorABMPedidoState={{
                            pantallaAnteriorABMPedido: pantallaAnteriorABMPedido,
                            setPantallaAnteriorABMPedido: setPantallaAnteriorABMPedido
                        }}
                        locationState={{
                            location: location,
                            city: city,
                            errorMsg: errorMsg,
                            time: time
                        }}
                        modalOpenDireccionFormState={{
                            modalOpenDireccionForm: modalOpenDireccionForm,
                            setModalOpenDireccionForm: setModalOpenDireccionForm
                        }}
                        dataFormDomicilioState={{
                            dataFormDomicilio: dataFormDomicilio,
                            setDataFormDomicilio: setDataFormDomicilio,
                        }}
                        dataFormDomicilioDestinoState={{
                            dataFormDomicilioDestino: dataFormDomicilioDestino,
                            setDataFormDomicilioDestino: setDataFormDomicilioDestino,
                        }}
                        modalOpenEncomiendaFormState={{
                            modalOpenEncomiendaForm: modalOpenEncomiendaForm,
                            setModalOpenEncomiendaForm: setModalOpenEncomiendaForm
                        }}
                        dataFormEncomiendaState={{
                            dataFormEncomienda: dataFormEncomienda,
                            setDataFormEncomienda: setDataFormEncomienda,
                        }}
                        dataSelectCadeteState={{
                            dataSelectCadete: dataSelectCadete,
                            setDataSelectCadete: setDataSelectCadete
                        }}
                        dataSelectCadeteSelectedState={{
                            dataSelectCadeteSelected: dataSelectCadeteSelected,
                            setDataSelectCadeteSelected: setDataSelectCadeteSelected
                        }}
                        misPedidosDataState={{
                            misPedidosData: misPedidosData,
                            setMisPedidosData: setMisPedidosData
                        }}
                    />

                    <FooterComponent 
                        userDataState={{
                            userData: userData, 
                            setUserData: setUserData
                        }}                           
                        colorModeState={{
                            colorMode: colorMode,
                            toggleColorMode: toggleColorMode
                        }}                        
                        tabButtonFooterState={{
                            selectedTabButtonFooter: selectedTabButtonFooter, 
                            setSelectedTabButtonFooter: setSelectedTabButtonFooter
                        }}
                    />

                    {/* MODALS */}
                    <TypeUserSelectionModalComponent  
                        userDataState={{
                            userData: userData, 
                            setUserData: setUserData
                        }}   
                        modalOpenSelectionTypeUserState={{
                            modalOpenSelectionTypeUser: modalOpenSelectionTypeUser, 
                            setModalOpenSelectionTypeUser: setModalOpenSelectionTypeUser
                        }}
                        colorModeState={{
                            colorMode: colorMode,
                            toggleColorMode: toggleColorMode
                        }}
                        modalOpenLoadingState={{
                            modalOpenLoading: modalOpenLoading, 
                            setModalOpenLoading: setModalOpenLoading
                        }}
                    />

                    <LoadingModalComponent 
                        modalOpenLoadingState={{
                            modalOpenLoading: modalOpenLoading, 
                            setModalOpenLoading: setModalOpenLoading
                        }}
                        colorModeState={{
                            colorMode: colorMode,
                            toggleColorMode: toggleColorMode
                        }}
                    />

                </BoxWithActualTheme>
            : null
        :
        <BoxWithActualTheme>
            <LoginView        
                userDataState={{
                    userData: userData, 
                    setUserData: setUserData
                }}
                isProfileImageLoadingState={{
                    isProfileImageLoading: isProfileImageLoading, 
                    setProfileImageLoadStatus: setProfileImageLoadStatus
                }}
                colorModeState={{
                    colorMode: colorMode,
                    toggleColorMode: toggleColorMode
                }}
                isLoggedinState={{
                    isLoggedin:isLoggedin,
                    setLoggedinStatus:setLoggedinStatus
                }}
                modalOpenSelectionTypeUserState={{
                    modalOpenSelectionTypeUser: modalOpenSelectionTypeUser, 
                    setModalOpenSelectionTypeUser: setModalOpenSelectionTypeUser
                }}
                modalOpenLoadingState={{
                    modalOpenLoading: modalOpenLoading, 
                    setModalOpenLoading: setModalOpenLoading
                }}
            />

            <LoadingModalComponent 
                modalOpenLoadingState={{
                    modalOpenLoading: modalOpenLoading, 
                    setModalOpenLoading: setModalOpenLoading
                }}
                colorModeState={{
                    colorMode: colorMode,
                    toggleColorMode: toggleColorMode
                }}
            />

            {/* LOADING MODAL VIEW */} 

        </BoxWithActualTheme>
    );
}