import React from 'react';
import { 
    Stack,
    Text,
    Box,
    HStack,
    Fab,
    VStack,
    Badge,
    Divider
} from 'native-base';

import { Icon } from 'react-native-elements';
import { ABMPedidoView } from './ABMPedidoView'
import { ListMisPedidosComponent } from '../components/ListMisPedidosComponent';
import database from '../services/firebase';

// Pedidos con cadete asignado y estado pendiente 
// ordenados por fecha de abm y estado pendiente siempre primero

export function MisPedidosView(props) {

    const userDataState = props.userDataState;
    const colorModeState = props.colorModeState;
    const tabButtonFooterState = props.tabButtonFooterState;
    const modalOpenLoadingState = props.modalOpenLoadingState;
    const abmPedidoDataState = props.abmPedidoDataState;
    const pantallaAnteriorABMPedidoState = props.pantallaAnteriorABMPedidoState;
    const locationState = props.locationState;
    const modalOpenDireccionFormState = props.modalOpenDireccionFormState;
    const dataFormDomicilioState = props.dataFormDomicilioState;
    const dataFormDomicilioDestinoState = props.dataFormDomicilioDestinoState;
    const modalOpenEncomiendaFormState = props.modalOpenEncomiendaFormState;
    const dataFormEncomiendaState = props.dataFormEncomiendaState;
    const dataSelectCadeteState = props.dataSelectCadeteState;
    const dataSelectCadeteSelectedState = props.dataSelectCadeteSelectedState;
    const misPedidosDataState = props.misPedidosDataState;

    const openABMPedidoNew = () => {
        dataFormDomicilioState.setDataFormDomicilio({
            tipoDomicilio: "Origen",
            calle: null,
            numero: null,
            barrio: null,
            localidad: null,
            provincia: null,
            pais: "Argentina",
            cordx: null,
            cordy: null,
            informacionAdicional: null,
            horario: null
        });

        dataFormDomicilioDestinoState.setDataFormDomicilioDestino({
            tipoDomicilio: "Destino",
            calle: null,
            numero: null,
            barrio: null,
            localidad: null,
            provincia: null,
            pais: "Argentina",
            cordx: null,
            cordy: null,
            informacionAdicional: null,
            horario: null,
            fechaEntrega: null,
            nombreReceptor: null,
            telefonoReceptor: null
        });

        dataFormEncomiendaState.setDataFormEncomienda({
            cantidadPaquetes: null,
            montoARecibir: null,
            pesoTotal: null,
            dimensionTotal: null,
            imagenAdjunta: null,
        });

        if (userDataState.userData.typeUser === "Cliente") {
            let dataCadetes = [];
            database.ref("cadetesPorCliente")
            .once('value', (snapshot) =>{
                snapshot.forEach((child)=>{
                    if (child) {
                        dataCadetes.push(child.val())
                    }
                })
                if ((dataCadetes.length != dataSelectCadeteState.dataSelectCadete.length) || dataSelectCadeteState.dataSelectCadete.length == 0) {
                    dataSelectCadeteState.dataSelectCadete = dataCadetes;
                    dataSelectCadeteState.setDataSelectCadete(dataSelectCadeteState.dataSelectCadete);
                }
            });
        }

        abmPedidoDataState.setABMPedidoData(true);
        dataSelectCadeteSelectedState.setDataSelectCadeteSelected({});
        pantallaAnteriorABMPedidoState.setPantallaAnteriorABMPedido("Mis Pedidos");
    }

    return (
        // Si no esta seteado el de ver pedido
        !abmPedidoDataState.abmPedidoData ?
            <Stack>
                {/* BOTONES FLOTANTES */}
                {userDataState.userData.typeUser === 'Cliente' &&
                <Box position="relative" w="100%"  >
                    <Fab my={20} variant={"ghost"}
                        onPress={() => openABMPedidoNew()}
                        position="absolute"
                        colorScheme={colorModeState.colorMode === 'dark' ? 'gray' : 'orange'} 
                        bg={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                        size="sm"
                        icon={  
                            <Icon name='plus'
                                type='font-awesome'
                                color={'#fc5404'}/>
                            } 
                        size="sm" 
                    />
                </Box>
                }
                {/* FIN BOTONES FLOTANTES  */}

                {/* BARRA DE PEDIDOS DESP ARMAR PARA CLIENTE Y CADETE EJEMPLAZO */}
                    <HStack mx={4} py={4} justifyContent='space-between'>
                        <VStack >
                            <HStack space={3}> 
                                <Badge colorScheme="success" py={0.5} borderRadius={100}> Confirmados: 10 </Badge>
                                <Badge colorScheme="yellow" py={0.5} borderRadius={100}> Pendientes: 10 </Badge>
                            </HStack>
                            {/* <HStack mx={4} alignItems='center' 
                                    bg={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                                    p={1}
                                    borderRadius={5}
                                > 
                                <Spinner color="warning.500" size={'sm'} />
                                <Button variant={"unstyled"} 
                                        _text={{
                                            fontWeight: "bold",
                                            color: '#fc5404'
                                        }}
                                        px={4} py={0} 
                                        onPress={() => alert('hola')}>
                                        Pepito juarez 2025 - 17:00 - Santi Pujol
                                </Button>                        
                            </HStack> */}
                        </VStack>
                    </HStack>

                <Divider  mx={0} 
                        bg={colorModeState.colorMode === 'dark' ? 'gray.600' : 'gray.400'} 
                        w={"100%"}
                />

                <HStack mx={2} py={2}>
                    <Text>   Buscar</Text>
                </HStack>
                {/* FIN BARRA DE PEDIDOS */}

                <ListMisPedidosComponent 
                    colorModeState={colorModeState}
                    userDataState={userDataState}
                    abmPedidoDataState={abmPedidoDataState}
                    pantallaAnteriorABMPedidoState={pantallaAnteriorABMPedidoState}
                    misPedidosDataState={misPedidosDataState}
                />

            </Stack>
        :
        // Si esta seteado el objeto ver pedido
        <Stack>
            <ABMPedidoView 
                userDataState={userDataState}
                abmPedidoDataState={abmPedidoDataState}
                colorModeState={colorModeState}
                tabButtonFooterState={tabButtonFooterState}
                locationState={locationState}
                modalOpenDireccionFormState={modalOpenDireccionFormState}
                modalOpenLoadingState={modalOpenLoadingState}
                dataFormDomicilioState={dataFormDomicilioState}
                dataFormDomicilioDestinoState={dataFormDomicilioDestinoState}
                modalOpenEncomiendaFormState={modalOpenEncomiendaFormState}
                dataFormEncomiendaState={dataFormEncomiendaState}
                misPedidosDataState={misPedidosDataState}
                dataSelectCadeteState={dataSelectCadeteState}
                dataSelectCadeteSelectedState={dataSelectCadeteSelectedState}
            />
        </Stack>
    );
}
