import React from 'react';
import { Text, Stack, Center } from 'native-base';

// Pedidos sin cadete asignado y estado pendiente 
// ordenados por fecha de abm y estado pendiente siempre primero
// En esta pantalla el usuario recibe modal para confirmar cuando el cadete le da a confirmar
// En el momento que el cliente recibe el modal de confirmacion en donde verifica al cadete,
// El pedido pasa a estado 'Pendiente confirmacion cliente', para que no le puedan seguir enviando modales
// El cliente puede aceptar y pasa a estado confirmado y adentro de mis pedidos, o puede cancelar,
// y el pedido sigue buscando cadete, el cadete solo puede apretar el boton una vez para esa fila,
// por ende no puede volver a mandarle y lo va a eliminar

// Buscar Cadete para Cliente y Buscar Cliente para Cadete - Resumido: Buscar Pedidos
export function SearchPedidosView() {
    return (
        <Stack  my={2} flex={1}>
            <Center>
                <Text>Search Pedidos</Text>
            </Center>
        </Stack>
    );
}
