import React, {useState, useEffect} from 'react';
import { Stack, 
    Text,
    FormControl,
    ScrollView,
    Button,
    HStack
 }  from 'native-base';
 import { DireccionFormModalComponent } from '../components/modals/DireccionFormModalComponent';
 import { EncomiendaFormModalComponent } from '../components/modals/EncomiendaFormModalComponent';
 import { SelectCadeteComponent } from '../components/SelectCadeteComponent';
 import database from '../services/firebase';
 import { Icon } from 'react-native-elements';

// Pedido nuevo si esta en true
// Si tiene id Ver pedido con acciones segun usuario
// Para el cadete muestra cliente y para el cliente cadete, lo demas todo lo mismo 
// nada mas que los botones distintos para cada usuario obvio

export function ABMPedidoView(props) {

    const userDataState = props.userDataState;
    const colorModeState = props.colorModeState;
    const abmPedidoDataState = props.abmPedidoDataState;
    const locationState = props.locationState;
    const modalOpenDireccionFormState = props.modalOpenDireccionFormState;
    const modalOpenLoadingState = props.modalOpenLoadingState;
    const dataFormDomicilioState = props.dataFormDomicilioState;
    const dataFormDomicilioDestinoState = props.dataFormDomicilioDestinoState;
    const modalOpenEncomiendaFormState = props.modalOpenEncomiendaFormState;
    const dataFormEncomiendaState = props.dataFormEncomiendaState;
    const dataSelectCadeteState = props.dataSelectCadeteState;
    const dataSelectCadeteSelectedState = props.dataSelectCadeteSelectedState;
    const misPedidosDataState = props.misPedidosDataState;

    const [errorsAbmPedido, setErrorsAbmPedido] = useState({});

    // TEST ADD CADETE POR CLIENTE
    const setCadeteByCliente = () => {

        var lastId = 0;

        database.ref("cadetesPorCliente").limitToLast(1).once('value', (snapshot) =>{
            snapshot.forEach((child)=>{
                if (child) {
                    lastId = Number(child.key);
                }
            })
        })

        const dataCadeteByCliente = {
            "id": lastId + 1,
            "idUserCliente":  userDataState.userData.id,
            "idUserCadete":  userDataState.userData.id,
            "lastDateLoggin": userDataState.userData.lastDateLoggin,
            "name": userDataState.userData.name,
            "picture": userDataState.userData.picture,
            "statusLoggin":  userDataState.userData.statusLoggin,
            "typeAccount": userDataState.userData.typeAccount,
            "typeUser": userDataState.userData.typeUser
        };

        // const dataCadeteByCliente = {
        //     "idUserCliente":  userDataState.userData.id,
        //     "email": "ags@gmail.com",
        //     "idUserCadete": "10226164949647148",
        //     "lastDateLoggin": userDataState.userData.lastDateLoggin,
        //     "name": "Agu Pujol",
        //     "picture": userDataState.userData.picture,
        //     "statusLoggin":  userDataState.userData.statusLoggin,
        //     "typeAccount": userDataState.userData.typeAccount,
        //     "typeUser": userDataState.userData.typeUser
        // };

        database.ref("cadetesPorCliente/" + (lastId + 1).toString()).set(dataCadeteByCliente).catch(alert);
        
    } 

    useEffect(() => {
        // setCadeteSelect()
        // setCadeteByCliente()
    } , []); 
    // TEST

    const ButtonDireccionOrigenComponent = () => {
        if (dataFormDomicilioState.dataFormDomicilio.calle == null) {
            return (<FormControl isRequired>
                        <FormControl.Label>Datos de retiro</FormControl.Label>
                        <Button variant="outline" 
                                colorScheme={colorModeState.colorMode === 'dark' ? 'gray' : 'gray'}
                                backgroundColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                                borderColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                                onPress={() => {
                                    modalOpenDireccionFormState.setModalOpenDireccionForm({value: true, type: "Origen", edit: false});
                                }}                            
                                _text={{
                                    fontWeight:"bold",
                                    color: colorModeState.colorMode === 'dark' ? 'gray.500' : 'gray.500'
                                }}
                            >
                            + Agregar datos de retiro
                        </Button>
                    </FormControl>
            );

        } else {
            return (<FormControl isRequired>
                        <FormControl.Label>Dirección de retiro</FormControl.Label>
                        <Button variant="outline" 
                                colorScheme={colorModeState.colorMode === 'dark' ? 'gray' : 'gray'}
                                backgroundColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                                borderColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                                onPress={() => {
                                    modalOpenDireccionFormState.setModalOpenDireccionForm({value: true, type: "Origen", edit: true});
                                }}                            
                            >
                            <Stack alignItems="center">
                                <Text bold>
                                    {dataFormDomicilioState.dataFormDomicilio.calle ? dataFormDomicilioState.dataFormDomicilio.calle : ""} 
                                    {dataFormDomicilioState.dataFormDomicilio.numero ? " " + dataFormDomicilioState.dataFormDomicilio.numero : ""}
                                    {dataFormDomicilioState.dataFormDomicilio.barrio ? ", Barrio " +  dataFormDomicilioState.dataFormDomicilio.barrio : ""}
                                </Text>  
                                <Text bold>                                
                                    {dataFormDomicilioState.dataFormDomicilio.localidad ? dataFormDomicilioState.dataFormDomicilio.localidad : ""}
                                    {dataFormDomicilioState.dataFormDomicilio.provincia ? ", " +  dataFormDomicilioState.dataFormDomicilio.provincia : ""}
                                </Text>   
                                {dataFormDomicilioState.dataFormDomicilio.horario != "" && dataFormDomicilioState.dataFormDomicilio.horario != null &&
                                    <Text bold>                                
                                        {dataFormDomicilioState.dataFormDomicilio.horario}
                                    </Text>
                                }
                                {dataFormDomicilioState.dataFormDomicilio.informacionAdicional != "" && dataFormDomicilioState.dataFormDomicilio.informacionAdicional != null &&
                                    <Text bold>                                
                                        {dataFormDomicilioState.dataFormDomicilio.informacionAdicional}
                                    </Text>   
                                }
                            </Stack>
                        </Button>
                    </FormControl>
            );        
        }                    
    }

    const ButtonDireccionEntregaComponent = () => {
        if (dataFormDomicilioDestinoState.dataFormDomicilioDestino.calle == null) {
            return (<FormControl isRequired>
                        <FormControl.Label>Datos de entrega</FormControl.Label>
                        <Button my={0} variant="outline" 
                            colorScheme={colorModeState.colorMode === 'dark' ? 'gray' : 'gray'}
                            backgroundColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                            borderColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                            onPress={() => {
                                modalOpenDireccionFormState.setModalOpenDireccionForm({value: true, type: "Destino"});
                            }}                            
                            _text={{
                                fontWeight:"bold",
                                color: colorModeState.colorMode === 'dark' ? 'gray.500' : 'gray.500'
                            }}  
                        >
                            + Agregar datos de entrega
                        </Button>
                    </FormControl>
            );

        } else {
            return (<FormControl isRequired>
                        <FormControl.Label>Datos de Entrega</FormControl.Label>

                        <Button my={0} variant="outline" 
                            colorScheme={colorModeState.colorMode === 'dark' ? 'gray' : 'gray'}
                            backgroundColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                            borderColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                            onPress={() => {
                                modalOpenDireccionFormState.setModalOpenDireccionForm({value: true, type: "Destino"});
                            }}                            
                            _text={{
                                fontWeight: "bold",
                                color: colorModeState.colorMode === 'dark' ? 'gray.500' : 'gray.500'
                            }}  
                        >
                            <Stack alignItems="center">
                                <Text bold>
                                    {dataFormDomicilioDestinoState.dataFormDomicilioDestino.calle ? dataFormDomicilioDestinoState.dataFormDomicilioDestino.calle : ""} 
                                    {dataFormDomicilioDestinoState.dataFormDomicilioDestino.numero ? " " + dataFormDomicilioDestinoState.dataFormDomicilioDestino.numero : ""}
                                    {dataFormDomicilioDestinoState.dataFormDomicilioDestino.barrio ? ", Barrio " +  dataFormDomicilioDestinoState.dataFormDomicilioDestino.barrio : ""}
                                </Text>      
                                <Text bold>                                
                                    {dataFormDomicilioDestinoState.dataFormDomicilioDestino.localidad ? dataFormDomicilioDestinoState.dataFormDomicilioDestino.localidad : ""}
                                    {dataFormDomicilioDestinoState.dataFormDomicilioDestino.provincia ? ", " +  dataFormDomicilioDestinoState.dataFormDomicilioDestino.provincia : ""}
                                </Text>   
                                <Text bold>                                
                                    {dataFormDomicilioDestinoState.dataFormDomicilioDestino.fechaEntrega ? dataFormDomicilioDestinoState.dataFormDomicilioDestino.fechaEntrega : ""}
                                    {dataFormDomicilioDestinoState.dataFormDomicilioDestino.horario ? ", " +  dataFormDomicilioDestinoState.dataFormDomicilioDestino.horario : ""}
                                </Text>   
                                
                                {dataFormDomicilioDestinoState.dataFormDomicilioDestino.nombreReceptor != "" && dataFormDomicilioDestinoState.dataFormDomicilioDestino.nombreReceptor != null &&
                                    <Text bold>                                
                                        {dataFormDomicilioDestinoState.dataFormDomicilioDestino.nombreReceptor != null ? dataFormDomicilioDestinoState.dataFormDomicilioDestino.nombreReceptor : ""}
                                        {dataFormDomicilioDestinoState.dataFormDomicilioDestino.telefonoReceptor != null ? ", " +  dataFormDomicilioDestinoState.dataFormDomicilioDestino.telefonoReceptor : ""}
                                    </Text>   
                                }
                                {dataFormDomicilioDestinoState.dataFormDomicilioDestino.informacionAdicional != "" && dataFormDomicilioDestinoState.dataFormDomicilioDestino.informacionAdicional != null &&
                                    <Text bold>                                
                                        {dataFormDomicilioDestinoState.dataFormDomicilioDestino.informacionAdicional}
                                    </Text>   
                                }
                            </Stack>
                        </Button>
                    </FormControl>
            );        
        }                    
    }

    const ButtonEncomiendaComponent = () => {
        if (dataFormEncomiendaState.dataFormEncomienda.cantidadPaquetes == null) {
            return (<FormControl isRequired>
                        <FormControl.Label>Datos de encomienda</FormControl.Label>
                        <Button my={0} variant="outline" 
                            colorScheme={colorModeState.colorMode === 'dark' ? 'gray' : 'gray'}
                            backgroundColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                            borderColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                            onPress={() => {
                                modalOpenEncomiendaFormState.setModalOpenEncomiendaForm(true);
                            }}                            
                            _text={{
                                fontWeight:"bold",
                                color: colorModeState.colorMode === 'dark' ? 'gray.500' : 'gray.500'
                            }}  
                        >
                            + Agregar datos de encomienda
                        </Button>
                    </FormControl>
            );

        } else {
            return (<FormControl isRequired>
                        <FormControl.Label>Datos de encomienda</FormControl.Label>

                        <Button my={0} variant="outline" 
                            colorScheme={colorModeState.colorMode === 'dark' ? 'gray' : 'gray'}
                            backgroundColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'} 
                            borderColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}                           
                            onPress={() => {
                                modalOpenEncomiendaFormState.setModalOpenEncomiendaForm(true);
                            }}                            
                            _text={{
                                fontWeight: "bold",
                                color: colorModeState.colorMode === 'dark' ? 'gray.500' : 'gray.400'
                            }}  
                        >
                            <Stack alignItems="center">
                                <Text bold>
                                    {dataFormEncomiendaState.dataFormEncomienda.cantidadPaquetes ? "Paquetes: " + dataFormEncomiendaState.dataFormEncomienda.cantidadPaquetes : ""} 
                                </Text>      
                                <Text bold>                                
                                    {dataFormEncomiendaState.dataFormEncomienda.montoARecibir ? "Monto a recibir: " + dataFormEncomiendaState.dataFormEncomienda.montoARecibir : ""} 
                                </Text>   
                                {dataFormEncomiendaState.dataFormEncomienda.pesoTotal != "" && dataFormEncomiendaState.dataFormEncomienda.pesoTotal != null &&
                                    <Text bold>                                
                                        {dataFormEncomiendaState.dataFormEncomienda.pesoTotal != null ? "Peso total: " + dataFormEncomiendaState.dataFormEncomienda.pesoTotal : ""}
                                    </Text>   
                                }
                                {dataFormEncomiendaState.dataFormEncomienda.dimensionTotal != "" && dataFormEncomiendaState.dataFormEncomienda.dimensionTotal != null &&
                                    <Text bold>                                
                                        {dataFormEncomiendaState.dataFormEncomienda.dimensionTotal != null ? "Dimensión total: " + dataFormEncomiendaState.dataFormEncomienda.dimensionTotal : ""}
                                    </Text>   
                                }
                            </Stack>
                        </Button>
                    </FormControl>
            );        
        }                    
    }
    
    const validate = () => {
        if (dataSelectCadeteSelectedState.dataSelectCadeteSelected == null) {
            setErrorsAbmPedido({
                ...errorsAbmPedido,
                userCadete: 'Cadete es requerido',
            });
            alert('Cadete es requerido')
            return false;
        }

        if (dataFormDomicilioState.dataFormDomicilio.calle == null) {
            setErrorsAbmPedido({
                ...errorsAbmPedido,
                direccionOrigen: 'Datos de retiro son requeridos',
            });
            alert('Datos de retiro son requeridos');
            return false;
        }

        if (dataFormDomicilioDestinoState.dataFormDomicilioDestino.calle == null) {
            setErrorsAbmPedido({
                ...errorsAbmPedido,
                direccionDestino: 'Datos de entrega son requeridos',
            });
            alert('Datos de entrega son requeridos');
            return false;
        }

        if (dataFormEncomiendaState.dataFormEncomienda.cantidadPaquetes == null) {
            setErrorsAbmPedido({
                ...errorsAbmPedido,
                cantidadPaquetes: 'Encomienda es requerida',
            });
            alert('Encomienda es requerida');
            return false;
        }

        return true;
    };

    const grabarNuevoPedido = () => {
        
        if (validate()) {
            
            var lastId = 0;

            // database.ref("pedidos").limitToLast(1).once('value', (snapshot) =>{
            //     snapshot.forEach((child)=>{
            //         if (child) {
            //             lastId = Number(child.key);
            //         }
            //     })
            // })

            let dataABMPedido = {
                "userCliente": userDataState.userData,
                "userCadete": dataSelectCadeteSelectedState.dataSelectCadeteSelected,
                "fechaAlta": new Date().toUTCString(),
                "fechaUltimaModif": new Date().toUTCString(),
                "estado": "Pendiente",
                "direccionOrigen": dataFormDomicilioState.dataFormDomicilio,
                "direccionDestino": dataFormDomicilioDestinoState.dataFormDomicilioDestino,
                "encomienda": dataFormEncomiendaState.dataFormEncomienda,
                "costoEnvio": 0, // Esto se va a calcular segun la información del cadete en su configuración
                                        // Por le momento cargado a mano cuando se confirma tomar un pedido, y el cliente lo tiene q aceptar
                "mensajes": null
            };

            database.ref("pedidos").push(dataABMPedido).catch(alert);

            abmPedidoDataState.setABMPedidoData(dataABMPedido);
            modalOpenLoadingState.setModalOpenLoading(true);
            setTimeout(() => {
                abmPedidoDataState.setABMPedidoData(null);
                modalOpenLoadingState.setModalOpenLoading(false);            
            }, 100);
        }

    }

    return (
        /* Si tiene id es porque es viene a ver o aplicar cambio de estado(
                -confirmar crear pedido para cliente (con confirmacion)
                -confirmar tomar pedido para cadete (con confirmacion)
                -editar pedido para cliente o cadete (modificar direccion de destino, y rango de horario), al modificarlo pasarlo a pendiente Modificado o pendiente de nuevo. 
                -cancelar pedido (con confirmacion) para ambos (con motivo y boton de wsp (aconsejandole que 
                                            por favor solucione el problmea antes de recomendar))
                                            Al final de cancelar pedido se recomienda
                -Mensaje para para cadete o cliente que dispara modal avisandole algo sobre el pedido, se va pisando la data.
                 apareciendole un mensaje a la otra persona que pregunta si Continua o Cancela pedido

                Pedido Retirado para Cadete
                Finalizar Pedido para cadete (con confirmacion) Al final de finalizar el pedido pedido se recomienda

            ) 
        */
        abmPedidoDataState.abmPedidoData.id > 0 ? 
            <Stack flex={1}
                    bg={colorModeState.colorMode === 'dark' ? 'gray.800' : 'gray.200'}  
                    alignItems="center" 
                    safeAreaBottom shadow={6}
                >
                <Text>Ver y cambiar de estado</Text>
            </Stack>
        : 
        // Si no tiene id es porque se esta creando
            // Si esta abriendo direccion muestro modal direccion
            modalOpenDireccionFormState.modalOpenDireccionForm.value ?
                <ScrollView>
                    <DireccionFormModalComponent
                        colorModeState={colorModeState}
                        modalOpenDireccionFormState={modalOpenDireccionFormState}
                        dataFormDomicilioState={dataFormDomicilioState}
                        dataFormDomicilioDestinoState={dataFormDomicilioDestinoState}
                        modalOpenLoadingState={modalOpenLoadingState}
                    />
                </ScrollView>
            :    
            // Si esta abriendo encomienda muestro modal encomienda
            modalOpenEncomiendaFormState.modalOpenEncomiendaForm ?
            <ScrollView>
                <EncomiendaFormModalComponent
                    colorModeState={colorModeState}
                    modalOpenEncomiendaFormState={modalOpenEncomiendaFormState}
                    dataFormEncomiendaState={dataFormEncomiendaState}
                    modalOpenLoadingState={modalOpenLoadingState}
                />
            </ScrollView>
            :  
            // Si esta cerrada la direccion o encomienda muestro vista seguir agregando
                <ScrollView>
                    <Stack space={4} px={4} py={2.5} safeArea >
                                
                        {/* <SelectTipoServicioComponent /> */}
                        <ButtonDireccionOrigenComponent />
                        <ButtonDireccionEntregaComponent />
                        <ButtonEncomiendaComponent />
                        <SelectCadeteComponent 
                            userDataState={userDataState}
                            colorModeState={colorModeState}
                            dataSelectCadeteState={dataSelectCadeteState}
                            dataSelectCadeteSelectedState={dataSelectCadeteSelectedState}
                        />

                        <Stack backgroundColor={colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'} 
                                borderRadius="50">
                            <Button  my={0} borderRadius="50"
                                    variant="ghost" 
                                    colorScheme={colorModeState.colorMode === 'dark' ? 'gray' : 'orange'} 
                                    onPress={() => {
                                        grabarNuevoPedido()
                                    }}                                       
                                >
                                <HStack>
                                    <Icon name='check'
                                        type='font-awesome'
                                        color='#fc5404'
                                    />
                                    <Text bold color={colorModeState.colorMode === 'dark' ? '#fc5404' : '#fc5404'}> Crear pedido</Text>
                                </HStack>
                            </Button>
                        </Stack>
                    </Stack>
                </ScrollView>       
    );
}

    /* ESTADOS
        Pendiente 
        Pendiente Modficado
        Confirmado
        Retirado
        Cancelado
        Finalizado
    */