import React from 'react';
import Constants from 'expo-constants';
import database from '../services/firebase';

import { 
    Text,
    Box,
    Image,
    Button,
    StatusBar,
    HStack,
    MoonIcon,
    SunIcon,
    Divider,
    Actionsheet,
    VStack,
    Stack,
    Avatar
} 
from 'native-base';

import { Icon } from 'react-native-elements';

export function HeaderView(props) {

    const isLoggedinState = props.isLoggedinState;
    const userDataState = props.userDataState;
    const colorModeState = props.colorModeState;
    const isProfileImageLoadingState = props.isProfileImageLoadingState;
    const actionSheetState = props.actionSheetState;
    const modalOpenLoadingState = props.modalOpenLoadingState;
    const tabButtonFooterState = props.tabButtonFooterState;
    const abmPedidoDataState = props.abmPedidoDataState;
    const pantallaAnteriorABMPedidoState = props.pantallaAnteriorABMPedidoState;
    const modalOpenDireccionFormState = props.modalOpenDireccionFormState;
    const modalOpenEncomiendaFormState = props.modalOpenEncomiendaFormState;
    
    const IconThemeModeButtonProfileAction = () => {
        if (colorModeState.colorMode == "light") 
            return (<MoonIcon color="#000000" w={6} />);
        return (<SunIcon color="#ffffff" w={6} />);
    };

    const ButtonCotizador = () => {
        if (userDataState.userData.typeUser === "Cadete") 
            return (<Actionsheet.Item py={4} 
                startIcon={
                    <Icon name='cog'
                    type='font-awesome'
                    color={colorModeState.colorMode === 'light' ? '#000000' : '#ffffff'} />
                }
                >
                <Text mx={4}>Ajustes <Text bold>Cotizador</Text></Text> 
            </Actionsheet.Item>);
        return null;
    };

    const changeTypeAccount = () => {
        modalOpenLoadingState.setModalOpenLoading(true);

        setTimeout(() => {
            if (userDataState.userData.typeAccount === "GOLD") {
                userDataState.userData.typeAccount = "FREE"
            } else {
                userDataState.userData.typeAccount = "GOLD";
            }
            userDataState.setUserData(userDataState.userData); 
            tabButtonFooterState.setSelectedTabButtonFooter("Mis Pedidos");
            modalOpenLoadingState.setModalOpenLoading(false);
        }, 100);
    }

    const changeTypeUser = () => {
        modalOpenLoadingState.setModalOpenLoading(true);

        setTimeout(() => {
            if (userDataState.userData.typeUser === "Cadete") {
                userDataState.userData.typeUser = "Cliente"
            } else {
                userDataState.userData.typeUser = "Cadete";
            }
            userDataState.setUserData(userDataState.userData); 

            modalOpenLoadingState.setModalOpenLoading(false);
        }, 100);
    }

    const logout = () => {
        modalOpenLoadingState.setModalOpenLoading(true);

        setTimeout(() => {
            isLoggedinState.setLoggedinStatus(false);

            // Seteamos ultimo acceso en base
            userDataState.userData.lastDateLoggin = new Date().toUTCString();
            userDataState.userData.statusLoggin = false;
            
            database.ref("users/" + userDataState.userData.id).set(userDataState.userData).catch(alert);
            userDataState.setUserData(null);

            isProfileImageLoadingState.setProfileImageLoadStatus(false);
            modalOpenLoadingState.setModalOpenLoading(false);
        }, 100);
    }

    const backTo = (pantalla) => {
        if (pantalla == "Mis Pedidos") {
            abmPedidoDataState.setABMPedidoData(null);
        }
        
        if (pantalla == "Crear Pedido") {
            modalOpenDireccionFormState.setModalOpenDireccionForm({value: false});
            modalOpenEncomiendaFormState.setModalOpenEncomiendaForm(false)
        }
    }

    return (<Stack>
                <StatusBar backgroundColor="#000000" barStyle="light-content" />
                <Box safeAreaTop backgroundColor="#3700B3" />

                <HStack bg={colorModeState.colorMode === 'dark' ? '#fc5404' : '#fc5404'} px={0} shadow={6} justifyContent='space-between' alignItems='center'>
                    
                    {/* FLECHA VOLVER si tiene data de ver pedido y esta parado en la misma pantalla que de donde viene muestro el boton */}
                    { abmPedidoDataState.abmPedidoData && tabButtonFooterState.selectedTabButtonFooter == pantallaAnteriorABMPedidoState.pantallaAnteriorABMPedido &&
                        <HStack mx={-1}>
                            <VStack >
                                <Button onPress={() => backTo((
                                        modalOpenDireccionFormState.modalOpenDireccionForm.value 
                                        || modalOpenEncomiendaFormState.modalOpenEncomiendaForm
                                        // Modales con || en true
                                        ) 
                                        ? "Crear Pedido" : "Mis Pedidos")
                                    }
                                    bg={colorModeState.colorMode === 'dark' ? '#fc5404' : '#fc5404'}
                                    variant={"unstyled"}
                                    >
                                    {(!modalOpenDireccionFormState.modalOpenDireccionForm.value 
                                        && !modalOpenEncomiendaFormState.modalOpenEncomiendaForm
                                        // Modales con && en false
                                        && abmPedidoDataState.abmPedidoData == true)
                                    ?
                                        <Icon name='close'
                                            type='font-awesome'
                                            color={'#ffffff'}
                                        />
                                    :
                                        <Icon name='arrow-left'
                                            type='font-awesome'
                                            color={'#ffffff'}
                                        />
                                    }
                                </Button>                        
                            </VStack>   
                            <HStack py={2.5}>

                                <Text color={"white"} mx={6} fontSize={22} bold>
                                    {modalOpenDireccionFormState.modalOpenDireccionForm.value &&
                                        "Datos de " + (modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? "Retiro" : "Entrega")
                                    }
                                    {modalOpenEncomiendaFormState.modalOpenEncomiendaForm &&
                                        "Datos de Encomienda"
                                    }

                                    
                                    {(!modalOpenDireccionFormState.modalOpenDireccionForm.value 
                                        && !modalOpenEncomiendaFormState.modalOpenEncomiendaForm
                                        // Modales con && en false
                                        ) 
                                        && abmPedidoDataState.abmPedidoData != true 
                                        && abmPedidoDataState.abmPedidoData.id != null &&
                                        "Pedido #" + abmPedidoDataState.abmPedidoData.id
                                    }
                                    {(!modalOpenDireccionFormState.modalOpenDireccionForm.value 
                                        && !modalOpenEncomiendaFormState.modalOpenEncomiendaForm
                                        // Modales con && en false
                                        )
                                        && abmPedidoDataState.abmPedidoData == true &&
                                        "Nuevo Pedido"
                                    }
                                </Text>  
                            </HStack>   
                        </HStack>   
                    } 

                    {/* LOGO LETRA si tiene data de pedido y esta en parado en otra pantalla muestro el logo */}
                    { abmPedidoDataState.abmPedidoData &&  tabButtonFooterState.selectedTabButtonFooter != pantallaAnteriorABMPedidoState.pantallaAnteriorABMPedido &&
                        <HStack>
                            <Image mx={-7} px={100} py={4} accessibilityLabel={"Imagen"}
                                source={{
                                    uri: Constants.manifest.extra.images.logoImageLetraUrlBlanca}
                                }
                            />
                        </HStack>   
                    }

                    {/* LOGO LETRA si no tiene data de pedido muestro*/}
                    { !abmPedidoDataState.abmPedidoData && 
                        <HStack>
                            <Image mx={-7} px={100} py={4} accessibilityLabel={"Imagen"}
                                source={{
                                    uri: Constants.manifest.extra.images.logoImageLetraUrlBlanca}
                                }
                            />
                        </HStack>   
                    }
                 
                    <HStack mx={-1} alignItems='center'>
                        <Box mx={-1} alignItems='center'>
                            <Text 
                                color={colorModeState.colorMode === 'dark' ? '#FFFFFF' : '#FFFFFF'} 
                                bold fontSize={11}> {userDataState.userData.typeUser} 
                            </Text>
                            <Text
                                borderRadius={60} fontSize={10} 
                                color={userDataState.userData.typeAccount === "FREE" ? '#777777' : '#777777'} 
                                bg={userDataState.userData.typeAccount === "GOLD" ? 'yellow.300' : 'green.300'} 
                                bold>
                                    {userDataState.userData.typeAccount != ""  ? "  " + userDataState.userData.typeAccount + "  " : "" }
                            </Text>
                        </Box>
                        <Button px={3} py={1.5} variant="unstyled" onPress={actionSheetState.onOpen}>
                            <Avatar source={{
                                        uri: userDataState.userData.picture.data.url
                                    }}
                                    onLoadEnd={() => isProfileImageLoadingState.setProfileImageLoadStatus(true)} 
                                    size={10}
                                >
                                {/* <Avatar.Badge bg={"red.200"} /> */}
                            </Avatar>
                        </Button>
                    </HStack>
                </HStack>

                {/* ACTIONS PROFILE VIEW */}
                <VStack space={4} >
                    <Actionsheet isOpen={actionSheetState.isOpen} onClose={actionSheetState.onClose} size="full">
                        <Actionsheet.Content >

                            <Actionsheet.Item py={4} 
                                startIcon={
                                    <Icon name='user-circle-o'
                                    type='font-awesome'
                                    color={colorModeState.colorMode === 'light' ? '#000000' : '#ffffff'} />
                                }
                                >
                                <Text mx={3}>Mi <Text bold>Perfil</Text></Text> 
                            </Actionsheet.Item>

                            <ButtonCotizador></ButtonCotizador>
                        
                            <Actionsheet.Item py={4} onPress={() => { colorModeState.toggleColorMode(); actionSheetState.onClose(); }}   
                                startIcon={
                                    <IconThemeModeButtonProfileAction />
                                }
                                >
                                <Text mx={3}>Activar <Text bold>{colorModeState.colorMode === 'light' ? 'Dark' : 'Light'} Mode</Text></Text>
                            </Actionsheet.Item>

                            <Actionsheet.Item py={4} onPress={() => { changeTypeAccount(); actionSheetState.onClose(); }}
                                startIcon={
                                    <Icon name='star'
                                    type='font-awesome'
                                    color={colorModeState.colorMode === 'light' ? '#000000' : '#ffffff'} />
                                }
                                >
                                <Text mx={3}>{userDataState.userData.typeAccount === 'GOLD' ? 'Desactivar' : 'Activar'} Cuenta <Text bold>Gold</Text></Text>
                            </Actionsheet.Item> 

                            <Actionsheet.Item py={4} onPress={() => { changeTypeUser(); actionSheetState.onClose(); }}
                                startIcon={
                                    <Icon name='arrow-right'
                                    type='font-awesome'
                                    color={colorModeState.colorMode === 'light' ? '#000000' : '#ffffff'} />
                                }
                                >
                                <Text mx={4}>Prueba como <Text bold>{userDataState.userData.typeUser === 'Cadete' ? 'Cliente' : 'Cadete'}</Text></Text>
                            </Actionsheet.Item> 

                            <Divider my={3} bg={'gray.500'} />
                            
                            <Actionsheet.Item py={4} onPress={() => { logout(); actionSheetState.onClose(); }}
                                startIcon={
                                    <Icon name='power-off'
                                    type='font-awesome'
                                    color={colorModeState.colorMode === 'light' ? '#000000' : '#ffffff'} />
                                }
                                >
                                <Text mx={4}>Cerrar Sesión</Text> 
                            </Actionsheet.Item>
                        </Actionsheet.Content>
                    </Actionsheet>
                </VStack>
                {/* FIN ACTIONS PROFILE VIEW  */}
            </Stack>
    );
}