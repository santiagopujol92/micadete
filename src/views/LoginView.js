import React from 'react';
import { SocialIcon} from 'react-native-elements';
import Constants from 'expo-constants';
import * as Facebook from 'expo-facebook';
import database from '../services/firebase';

import firebase from 'firebase';
import { 
    Text,
    Box,
    Image,
    Button,
    StatusBar,
    Center,
    MoonIcon,
    SunIcon,
    Divider,
    Stack,
    Heading,
} 
from 'native-base';

export function LoginView(props) {

    const userDataState = props.userDataState;
    const isLoggedinState = props.isLoggedinState;
    const colorModeState = props.colorModeState;
    const modalOpenLoadingState = props.modalOpenLoadingState;
    const modalOpenSelectionTypeUserState = props.modalOpenSelectionTypeUserState;
    const isProfileImageLoadingState = props.isProfileImageLoadingState;

    const googleLogIn = () => {
        modalOpenLoadingState.setModalOpenLoading(true);
        
        // Loading
        setTimeout(() => {

            // Setear login de google
            const dataUserdeGoogle = {
                "email": "santi04_cat@hotmail.com",
                "id": "10226164949647147",
                "name": "Santi Pujol",
                "picture": {
                    "data": {
                    "height": 706,
                    "is_silhouette": false,
                    "url": "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=10226164949647147&height=500&ext=1630884282&hash=AeSDHklFj4e5rrZNj0U",
                    "width": 706,
                    },
                },
                "typeUser": "",
                "typeAccount": "FREE",
                "lastDateLoggin": new Date().toUTCString(),
                "statusLoggin": true
            };
            
            saveUserDB(dataUserdeGoogle);
        }, 100);
    }

    const saveUserDB = (dataUser) => {
        if (dataUser) {
            // Busco el usuario
            database.ref('/users/' + dataUser.id).once('value').then(snapshot => {    
                isLoggedinState.setLoggedinStatus(true);

                // Si no existe el usuario en DB lo agrego a la base iniciando FREE y status de login, sin typeUser seteado
                if (snapshot.val() == null) {
                    database.ref("users/" + dataUser.id).set(dataUser).catch(alert);
                    userDataState.setUserData(dataUser)

                    // Si no tiene tipo de usuario abrimos modal de seleccion 
                    if (dataUser.typeUser == "" || dataUser.typeUser == null) {
                        modalOpenSelectionTypeUserState.setModalOpenSelectionTypeUser(true);
                    }
                }

                // Si existe seteo el data en el login y guardo en base el logeo
                else {
                    var dataUserDB = snapshot.val();
                    dataUserDB.lastDateLoggin = new Date().toUTCString();
                    dataUserDB.statusLoggin = true;
                    userDataState.setUserData(dataUserDB);
                    database.ref("users/" + dataUserDB.id).set(dataUserDB).catch(alert);
                    
                    if (dataUserDB.typeUser == "" || dataUserDB.typeUser == null) {
                        modalOpenSelectionTypeUserState.setModalOpenSelectionTypeUser(true);
                    }
                }
                
                isProfileImageLoadingState.setProfileImageLoadStatus(true);
                modalOpenLoadingState.setModalOpenLoading(false);
            });
        }
    }

    const facebookLogIn = async () => {
        // Si tiene data continuo, y no logeo
        try {
            modalOpenLoadingState.setModalOpenLoading(true);
            await Facebook.initializeAsync({appId: Constants.manifest.extra.facebook.appId});
            const { type, token, expires, permissions, declinedPermissions } = await Facebook.logInWithReadPermissionsAsync({permissions: ["public_profile", "email"]});

            if (type === 'success') {
                // Iniciamos sesion en firebase
                await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);  // Set persistent auth state
                const credential = firebase.auth.FacebookAuthProvider.credential(token);
                const facebookProfileData = await firebase.auth().signInWithCredential(credential);  // Sign in with Facebook credential

                // Get the user's name using Facebook's Graph API
                fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`).then(response => response.json()).then(data => {

                    saveUserDB(data);

                }).catch(e => console.log(e))

            } else {
                modalOpenLoadingState.setModalOpenLoading(false);
            }

        } catch ({ message }) {
            modalOpenLoadingState.setModalOpenLoading(false);
            alert('Facebook Login Error, intente nuevamente');
        }
    }

    const IconThemeModeButton = () => {
        if (colorModeState.colorMode == "light") 
            return (<MoonIcon color="#ffffff"/>);
        return (<SunIcon color="rgb(251, 146, 60)"/>);
    };


    return (<Stack flex={1}>
                <StatusBar backgroundColor="#000000" barStyle="light-content" />

                {/* HEADING LOGIN */}
                <Heading my={10} flex={1}
                    alignSelf={{
                        base: "center",
                        md: "flex-start",
                    }}
                    >

                    <Stack  alignItems='center' >
                        <Stack >
                            <Image p={10} accessibilityLabel={"Imagen"} 
                                source={{
                                    uri: Constants.manifest.extra.images.logoImageTransUrl,
                                }}
                            />
                        </Stack>
                        <Stack py={1}>
                            <Image accessibilityLabel={"Imagen"}
                                px={120} py={5}
                                source={{
                                    uri: colorModeState.colorMode === 'light' ?  Constants.manifest.extra.images.logoImageLetraUrlNegra :  Constants.manifest.extra.images.logoImageLetraUrlBlanca}
                                }
                            />
                        </Stack>
                        <Stack my={4} alignItems='center'>
                            <Text>¡Crece con nosotros y construye un perfil comercial  </Text>
                            <Text>con tus pedidos de cadetería como</Text>
                            <Text><Text bold>Cadete</Text> o <Text bold>Cliente</Text>!</Text>
                        </Stack>
                    </Stack>
                </Heading>
                {/* FIN HEADING LOGIN */}
                

                {/* BOTONES LOGIN */}
                <Stack mx={4} my={0}  >
                    <Box>
                        <SocialIcon
                            title='Conectarse con Facebook'
                            button
                            type='facebook'
                            onPress={facebookLogIn} 
                        />
                    </Box>
                    <Box> 
                        <SocialIcon
                            title='Conectarse con Google'
                            button
                            type='google'
                            onPress={googleLogIn}
                        />
                    </Box>
                </Stack>
                {/* FIN BOTONES LOGIN */}

                <Divider 
                    mx={6} my={6} 
                    bg={colorModeState.colorMode === 'dark' ? 'gray.600' : 'gray.400'} 
                    w={"88%"}
                />

                {/* FOOTER LOGIN  */}
                <Stack flex={1} safeAreaTop >
                    <Center my={4} py={10}>
                        <Box>
                            <Button variant="ghost" 
                                colorScheme={colorModeState.colorMode === 'light' ? 'gray.100' : 'gray'} 
                                borderRadius={100}
                                p={2}
                                bg={colorModeState.colorMode === 'light' ? 'gray.900' : 'gray.100'} 
                                onPress={() => { colorModeState.toggleColorMode(); }}>
                                <IconThemeModeButton />
                            </Button> 
                        </Box>
                        <Box my={12}>
                            <Text>
                                <Text fontSize={'xs'} bold>MiCadete</Text> 
                                <Text fontSize={'xs'}> App by</Text>
                                <Text fontSize={'xs'} bold> Santiago Pujol</Text> 
                                <Text fontSize={'xs'}> ©</Text>
                                <Text fontSize={'xs'} bold> 2021</Text>
                            </Text>
                        </Box>
                    </Center>
                </Stack>
                {/* FIN FOOTER LOGIN */}            
            </Stack>
    );
}