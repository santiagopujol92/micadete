import React from 'react';
import { SearchPedidosView } from './SearchPedidosView';
import { MisPedidosView } from './MisPedidosView';
import { RankingView } from './RankingView';
import { Stack }  from 'native-base';

export function MainBodyView(props) {

    const userDataState = props.userDataState;
    const colorModeState = props.colorModeState;
    const tabButtonFooterState = props.tabButtonFooterState;
    const modalOpenLoadingState = props.modalOpenLoadingState;
    const abmPedidoDataState = props.abmPedidoDataState;
    const pantallaAnteriorABMPedidoState = props.pantallaAnteriorABMPedidoState;
    const locationState = props.locationState;
    const modalOpenDireccionFormState = props.modalOpenDireccionFormState;
    const dataFormDomicilioState = props.dataFormDomicilioState;
    const dataFormDomicilioDestinoState = props.dataFormDomicilioDestinoState;
    const modalOpenEncomiendaFormState = props.modalOpenEncomiendaFormState;
    const dataFormEncomiendaState = props.dataFormEncomiendaState;
    const dataSelectCadeteState = props.dataSelectCadeteState;
    const dataSelectCadeteSelectedState = props.dataSelectCadeteSelectedState;
    const misPedidosDataState = props.misPedidosDataState;

    const SelectedView = () => {
        switch (tabButtonFooterState.selectedTabButtonFooter) {
            case "Buscar": return <SearchPedidosView
                                userDataState={userDataState}
                                colorModeState={colorModeState}
                                tabButtonFooterState={tabButtonFooterState}
                                modalOpenLoadingState={modalOpenLoadingState}
                            />;
            case "Mis Pedidos": 
            return <MisPedidosView
                                userDataState={userDataState}
                                colorModeState={colorModeState}
                                tabButtonFooterState={tabButtonFooterState}
                                modalOpenLoadingState={modalOpenLoadingState}
                                abmPedidoDataState={abmPedidoDataState}
                                pantallaAnteriorABMPedidoState={pantallaAnteriorABMPedidoState}
                                locationState={locationState}
                                modalOpenDireccionFormState={modalOpenDireccionFormState}
                                dataFormDomicilioState={dataFormDomicilioState}
                                dataFormDomicilioDestinoState={dataFormDomicilioDestinoState}
                                modalOpenEncomiendaFormState={modalOpenEncomiendaFormState}
                                dataFormEncomiendaState={dataFormEncomiendaState}
                                dataSelectCadeteState={dataSelectCadeteState}
                                dataSelectCadeteSelectedState={dataSelectCadeteSelectedState}
                                misPedidosDataState={misPedidosDataState}
                            />;
            case "Ranking": return <RankingView
                                userDataState={userDataState}
                                colorModeState={colorModeState}
                                tabButtonFooterState={tabButtonFooterState}
                                modalOpenLoadingState={modalOpenLoadingState}
                            />;
            default: return null;
        }
    };

    return (<Stack flex={1}>
                <SelectedView />
            </Stack>
    );
}