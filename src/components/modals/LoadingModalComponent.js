import React from 'react';  
import { Modal, Heading, Spinner, HStack, Center } from 'native-base';  

export class LoadingModalComponent extends React.Component {  
  
    constructor(props) {  
        super(props);
        this.modalOpenLoadingState = props.modalOpenLoadingState;
        this.colorModeState = props.colorModeState;
    }  
    
    render() {
        return (  
            <Modal isOpen={this.modalOpenLoadingState.modalOpenLoading} px={20} size={"sm"} >
                <Modal.Content >
                    <Modal.Body>
                        <Center flex={1}>
                            <HStack space={2}>
                                <Spinner size={17} 
                                    color={this.colorModeState.colorMode === 'dark' ? 'gray.400' : 'gray.600'} 
                                    accessibilityLabel="Loading posts" 
                                />
                                <Heading size={"xs"}
                                    color={this.colorModeState.colorMode === 'dark' ? 'gray.400' : 'gray.600'} 
                                    >
                                    Cargando
                                </Heading>
                            </HStack>
                        </Center>
                    </Modal.Body>
                </Modal.Content>
            </Modal>
        );  
    }  
}  
