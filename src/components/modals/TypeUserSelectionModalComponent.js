import React from 'react';  
import { Modal, Button, Divider, Center, Text, Box } from 'native-base';  
import database from '../../services/firebase';

export class TypeUserSelectionModalComponent extends React.Component {  
  
    constructor(props) {  
        super(props);
        this.userDataState = props.userDataState;
        this.modalOpenSelectionTypeUserState = props.modalOpenSelectionTypeUserState;
        this.colorModeState = props.colorModeState;
        this.modalOpenLoadingState = props.modalOpenLoadingState;
    }  
    
    render() {

        const setTypeUserState = (typeUser) => {
            this.modalOpenSelectionTypeUserState.setModalOpenSelectionTypeUser(false);
            this.modalOpenLoadingState.setModalOpenLoading(true);

            setTimeout(() => {
                this.userDataState.userData.typeUser = typeUser;
                this.userDataState.setUserData(this.userDataState.userData);
                database.ref("users/" + this.userDataState.userData.id).set(this.userDataState.userData).catch(alert);

                this.modalOpenLoadingState.setModalOpenLoading(false);
            }, 200);

        }

        return (  
            <Modal isOpen={this.modalOpenSelectionTypeUserState.modalOpenSelectionTypeUser} size={"md"}>
                <Modal.Content>
                    <Modal.Header>
                        <Box>
                            <Text fontSize="xl" >Hola <Text fontSize="xl" bold>{this.userDataState.userData.name}</Text> !</Text>
                        </Box>
                    </Modal.Header>
                    <Modal.Body>
                        <Box _text={{
                            fontSize: "md",
                            color: this.colorModeState.colorMode === 'dark' ? 'gray.100' : 'gray.900',
                        }}>
                            Antes de comenzar, es necesario seleccionar tu perfil de negocio:
                        </Box>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button.Group >
                            <Button variant="ghost" 
                                colorScheme={this.colorModeState.colorMode === 'dark' ? 'gray' : 'orange'}
                                onPress={() => {
                                    setTypeUserState("Cadete")
                                }}                            
                                _text={{
                                    fontWeight: "bold",
                                    color: '#fc5404'
                                }}
                            >
                                Cadete
                            </Button>
                    
                            <Button variant="ghost"                                 
                                colorScheme={this.colorModeState.colorMode === 'dark' ? 'gray' : 'orange'}
                                onPress={() => {
                                    setTypeUserState("Cliente")
                                }}
                                _text={{
                                    fontWeight: "bold",
                                    color: '#fc5404'
                                }}
                            >
                                Cliente
                            </Button>
                        </Button.Group>
                    </Modal.Footer>
                </Modal.Content>
            </Modal>
        );  
    }  
}  
