import React, {useState} from 'react';  
import { Stack, 
    FormControl,
    Input,
    Button,
    ScrollView
 }  from 'native-base';

// Falso modal para cargar encomienda
export function EncomiendaFormModalComponent(props) {

    const modalOpenEncomiendaFormState = props.modalOpenEncomiendaFormState;
    const dataFormEncomiendaState = props.dataFormEncomiendaState;
    const modalOpenLoadingState = props.modalOpenLoadingState;

    const [formDataEncomienda, setFormDataEncomienda] = useState({
        cantidadPaquetes: dataFormEncomiendaState.dataFormEncomienda.cantidadPaquetes != null ? dataFormEncomiendaState.dataFormEncomienda.cantidadPaquetes : null,
        montoARecibir: dataFormEncomiendaState.dataFormEncomienda.montoARecibir != null ? dataFormEncomiendaState.dataFormEncomienda.montoARecibir : null,
        pesoTotal: dataFormEncomiendaState.dataFormEncomienda.pesoTotal != null ? dataFormEncomiendaState.dataFormEncomienda.pesoTotal : null,
        dimensionTotal: dataFormEncomiendaState.dataFormEncomienda.dimensionTotal != null ? dataFormEncomiendaState.dataFormEncomienda.dimensionTotal : null,
        imagenAdjunta: dataFormEncomiendaState.dataFormEncomienda.imagenAdjunta != null ? dataFormEncomiendaState.dataFormEncomienda.imagenAdjunta : null,
    });
    const [errorsEncomienda, setErrorsEncomienda] = useState({});

    const validate = () => {
        if (formDataEncomienda.cantidadPaquetes == null || formDataEncomienda.cantidadPaquetes == "") {
            setErrorsEncomienda({
                ...errorsEncomienda,
                cantidadPaquetes: 'Cantidad de paquetes es requerido',
            });
            return false;
        }

        if (formDataEncomienda.montoARecibir == null || formDataEncomienda.montoARecibir == "") {
            setErrorsEncomienda({
                ...errorsEncomienda,
                montoARecibir: 'Monto a recibir es requerido',
            });
            return false;
        }

        return true;
    };

  const onSubmit = (dataForm) => {
    if (validate()) {
        dataFormEncomiendaState.setDataFormEncomienda(dataForm)

        modalOpenLoadingState.setModalOpenLoading(true);
        setTimeout(() => {
            modalOpenEncomiendaFormState.setModalOpenEncomiendaForm(false)
            modalOpenLoadingState.setModalOpenLoading(false);            
        }, 100);
    }
  };

    return (
        <Stack isOpen={modalOpenEncomiendaFormState.modalOpenEncomiendaForm} 
            mx={4} 
            space={4}
            py={2.5} 
            justifyContent='space-between'
        >
            <ScrollView>
                <Stack space={4}>
                    <FormControl isRequired isInvalid={'cantidadPaquetes' in errorsEncomienda}>
                        <FormControl.Label>Cantidad de paquetes</FormControl.Label> 
                        <Input
                            placeholder="Ingrese cantidad de paquetes"
                            value={(formDataEncomienda.cantidadPaquetes)}
                            onChangeText={(value) => setFormDataEncomienda({ ...formDataEncomienda, cantidadPaquetes: value })}
                            mt={1}
                        />
                        {'cantidadPaquetes' in errorsEncomienda ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errorsEncomienda.cantidadPaquetes}</FormControl.ErrorMessage>
                        : 
                            null
                        } 
                    </FormControl>

                    <FormControl isRequired isInvalid={'montoARecibir' in errorsEncomienda}>
                        <FormControl.Label>Monto a recibir</FormControl.Label> 
                        <Input
                            placeholder="Ingrese monto a recibir"
                            value={(formDataEncomienda.montoARecibir)}
                            onChangeText={(value) => setFormDataEncomienda({ ...formDataEncomienda, montoARecibir: value })}
                            mt={1}
                        />
                        {'montoARecibir' in errorsEncomienda ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errorsEncomienda.montoARecibir}</FormControl.ErrorMessage>
                        : 
                            null
                        } 
                    </FormControl>
                    

                    <FormControl isInvalid={'pesoTotal' in errorsEncomienda}>
                        <FormControl.Label>Peso total</FormControl.Label> 
                        <Input
                            placeholder="Ingrese peso total"
                            value={(formDataEncomienda.pesoTotal)}
                            onChangeText={(value) => setFormDataEncomienda({ ...formDataEncomienda, pesoTotal: value })}
                            mt={1}
                        />
                        {'pesoTotal' in errorsEncomienda ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errorsEncomienda.pesoTotal}</FormControl.ErrorMessage>
                        : 
                            null
                        } 
                    </FormControl>

                    <FormControl isInvalid={'dimensionTotal' in errorsEncomienda}>
                        <FormControl.Label>Dimensión total</FormControl.Label> 
                        <Input
                            placeholder="Ingrese dimensión total"
                            value={(formDataEncomienda.dimensionTotal)}
                            onChangeText={(value) => setFormDataEncomienda({ ...formDataEncomienda, dimensionTotal: value })}
                            mt={1}
                        />
                        {'dimensionTotal' in errorsEncomienda ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errorsEncomienda.dimensionTotal}</FormControl.ErrorMessage>
                        : 
                            null
                        } 
                    </FormControl>
                    
                </Stack>

                <Stack py={3}>
                    <Button variant="ghost" 
                        colorScheme={'gray'} 
                        onPress={() => {
                            onSubmit(formDataEncomienda)
                        }}                           
                        _text={{
                            fontWeight: "bold",
                            color: '#fc5404'
                        }}
                    >
                        Guardar
                    </Button>
                </Stack>
            </ScrollView>
        </Stack>
    );   
}


