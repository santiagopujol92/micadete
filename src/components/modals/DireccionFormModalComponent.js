import React, {useState} from 'react';  
import { Stack, 
    FormControl,
    Input,
    Button,
    ScrollView
 }  from 'native-base';

 import * as Location from 'expo-location';

// Falso modal para cargar direccion origen o destino
export function DireccionFormModalComponent(props) {

    const locationState = props.locationState;
    const modalOpenDireccionFormState = props.modalOpenDireccionFormState;
    const dataFormDomicilioState = props.dataFormDomicilioState;
    const dataFormDomicilioDestinoState = props.dataFormDomicilioDestinoState;
    const modalOpenLoadingState = props.modalOpenLoadingState;

    const [formData, setFormData] = useState({
        tipoDomicilio: modalOpenDireccionFormState.modalOpenDireccionForm.type,
            calle: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.calle : dataFormDomicilioDestinoState.dataFormDomicilioDestino.calle,
            numero: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.numero : dataFormDomicilioDestinoState.dataFormDomicilioDestino.numero,
            barrio: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.barrio : dataFormDomicilioDestinoState.dataFormDomicilioDestino.barrio,
            localidad: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.localidad : dataFormDomicilioDestinoState.dataFormDomicilioDestino.localidad,
            provincia: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.provincia : dataFormDomicilioDestinoState.dataFormDomicilioDestino.provincia,
            pais: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.pais : dataFormDomicilioDestinoState.dataFormDomicilioDestino.pais,
            cordx: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.cordx : dataFormDomicilioDestinoState.dataFormDomicilioDestino.cordx,
            cordy: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.cordy : dataFormDomicilioDestinoState.dataFormDomicilioDestino.cordy,
            horario: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.horario : dataFormDomicilioDestinoState.dataFormDomicilioDestino.horario,
            informacionAdicional: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? dataFormDomicilioState.dataFormDomicilio.informacionAdicional : dataFormDomicilioDestinoState.dataFormDomicilioDestino.informacionAdicional,
            fechaEntrega: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? null : dataFormDomicilioDestinoState.dataFormDomicilioDestino.fechaEntrega,
            nombreReceptor: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? null : dataFormDomicilioDestinoState.dataFormDomicilioDestino.nombreReceptor,
            telefonoReceptor: modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen" ? null : dataFormDomicilioDestinoState.dataFormDomicilioDestino.telefonoReceptor,
        }
    );    

    const [errors, setErrors] = useState({});

    // Generar las cordenadas de esta direccion con location y setearlas
    const getCoordsDireccionOrigen = (dataForm) => {
        if (dataForm.calle != null 
            && dataForm.numero != null) {
                // Aca se deben ir a buscar las cord a travez de la data del form
                return { x: "Test x", y: "Test y"}; // test
        }
    }

    const validate = () => {
        if (formData.calle == null || formData.calle == "") {
            setErrors({
                ...errors,
                calle: 'Calle es requerido',
            });
            return false;
        }

        if (formData.numero == null || formData.numero == "") {
            setErrors({
                ...errors,
                numero: 'Número es requerido',
            });
            return false;
        }

        if (formData.barrio == null || formData.barrio == "") {
            setErrors({
                ...errors,
                barrio: 'Barrio es requerido',
            });
            return false;
        }

        if (formData.localidad == null || formData.localidad == "") {
            setErrors({
                ...errors,
                localidad: 'Localidad es requerido',
            });
            return false;
        }

        if (formData.provincia == null || formData.provincia == "") {
            setErrors({
                ...errors,
                provincia: 'Provincia es requerido',
            });
            return false;
        }

        if (formData.horario != null && formData.horario.toString().length > 100) {
            setErrors({
                ...errors,
                horario: 'Horario demasiado largo',
            });
            return false;
        }

        if (formData.informacionAdicional != null && formData.informacionAdicional.toString().length > 100) {
            setErrors({
                ...errors,
                informacionAdicional: 'Información adicional demasiado larga',
            });
            return false;
        }

        if (modalOpenDireccionFormState.modalOpenDireccionForm.type == "Destino") {
            if (formData.fechaEntrega == null || formData.fechaEntrega == "") {
                setErrors({
                    ...errors,
                    fechaEntrega: 'Fecha entrega es requerido',
                });
                return false;
            }
            if (formData.nombreReceptor != null && formData.nombreReceptor.toString().length > 100) {
                setErrors({
                    ...errors,
                    nombreReceptor: 'Nombre receptor demasiado largo',
                });
                return false;
            }
            if (formData.telefonoReceptor != null && formData.telefonoReceptor.toString().length > 50) {
                setErrors({
                    ...errors,
                    telefonoReceptor: 'Teléfono receptor demasiado largo',
                });
                return false;
            }
        }

        return true;
    };

  const onSubmit = (dataForm) => {

    if (validate()) {
        // Buscamos las cordenadas y setamos al temporal
        if (dataForm.calle != null && dataForm != null) {
            let coordsOrigen = getCoordsDireccionOrigen(dataForm);
            dataForm.cordx = coordsOrigen.x;
            dataForm.cordy = coordsOrigen.y;
        }

        // Guardamos la data segun corresponda
        if (modalOpenDireccionFormState.modalOpenDireccionForm.type == "Origen") {
            dataFormDomicilioState.setDataFormDomicilio(dataForm)
        } else if (modalOpenDireccionFormState.modalOpenDireccionForm.type == "Destino") {
            dataFormDomicilioDestinoState.setDataFormDomicilioDestino(dataForm)
        }

        modalOpenLoadingState.setModalOpenLoading(true);
        setTimeout(() => {
            modalOpenDireccionFormState.setModalOpenDireccionForm({value: false})
            modalOpenLoadingState.setModalOpenLoading(false);            
        }, 100);
    }
  };

    return (
        <Stack isOpen={modalOpenDireccionFormState.modalOpenDireccionForm.value} 
            mx={4} 
            space={4}
            py={2.5} 
            justifyContent='space-between'
        >
            <ScrollView>
                <Stack space={4}>
                    <FormControl isRequired isInvalid={'calle' in errors}>
                        <FormControl.Label>Calle</FormControl.Label> 
                        <Input
                            placeholder="Ingrese calle"
                            value={(formData.calle)}
                            onChangeText={(value) => setFormData({ ...formData, calle: value })}
                            mt={1}
                        />
                        {'calle' in errors ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.calle}</FormControl.ErrorMessage>
                        : 
                            null
                        } 
                    </FormControl>

                    <FormControl isRequired isInvalid={'numero' in errors}>
                        <FormControl.Label>Número</FormControl.Label>
                        <Input keyboardType='numeric'
                            placeholder="Ingrese número"
                            value={formData.numero}
                            onChangeText={(value) => setFormData({ ...formData, numero: value })}
                            mt={1}
                        />
                        {'numero' in errors ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.numero}</FormControl.ErrorMessage>
                        : 
                            null
                        } 
                    </FormControl>
                    
                    <FormControl isRequired isInvalid={'barrio' in errors}>
                        <FormControl.Label>Barrio</FormControl.Label>
                        <Input
                            placeholder="Ingrese barrio"
                            value={formData.barrio}
                            onChangeText={(value) => setFormData({ ...formData, barrio: value })}
                            mt={1}
                        />
                        {'barrio' in errors ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.barrio}</FormControl.ErrorMessage>
                        : 
                            null
                        }
                    </FormControl>

                    <FormControl isRequired isInvalid={'localidad' in errors}>
                        <FormControl.Label>Localidad</FormControl.Label>
                        <Input
                            placeholder="Ingrese localidad"
                            value={formData.localidad}
                            onChangeText={(value) => setFormData({ ...formData, localidad: value })}
                            mt={1}
                        />
                        {'localidad' in errors ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.localidad}</FormControl.ErrorMessage>
                        : 
                            null
                        }
                    </FormControl>

                    <FormControl isRequired isInvalid={'provincia' in errors}>
                        <FormControl.Label>Provincia</FormControl.Label>
                        <Input
                            placeholder="Ingrese provincia"
                            value={formData.provincia}
                            onChangeText={(value) => setFormData({ ...formData, provincia: value })}
                            mt={1}
                        />
                        {'provincia' in errors ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.provincia}</FormControl.ErrorMessage>
                        : 
                            null
                        }  
                    </FormControl>

                    { modalOpenDireccionFormState.modalOpenDireccionForm.type == "Destino" ? 
                        <FormControl isRequired isInvalid={'fechaEntrega' in errors}>
                            <FormControl.Label>Fecha Entrega</FormControl.Label>
                            <Input
                                placeholder="Ingrese fecha entrega"
                                value={formData.fechaEntrega}
                                onChangeText={(value) => setFormData({ ...formData, fechaEntrega: value })}
                                mt={1}
                            />
                            {'fechaEntrega' in errors ?
                                <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.fechaEntrega}</FormControl.ErrorMessage>
                            : 
                                null
                            }                      
                        </FormControl>
                    : null
                    }

                    <FormControl isInvalid={'horario' in errors}>
                        <FormControl.Label>Horario</FormControl.Label>
                        <Input
                            placeholder="Ingrese rango horario"
                            value={formData.horario}
                            onChangeText={(value) => setFormData({ ...formData, horario: value })}
                            mt={1}
                        />
                        {'horario' in errors ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.horario}</FormControl.ErrorMessage>
                        : 
                            null
                        }                      
                    </FormControl>
                    


                    { modalOpenDireccionFormState.modalOpenDireccionForm.type == "Destino" ? 
                        <FormControl isInvalid={'nombreReceptor' in errors}>
                            <FormControl.Label>Nombre receptor</FormControl.Label>
                            <Input
                                placeholder="Ingrese nombre receptor"
                                value={formData.nombreReceptor}
                                onChangeText={(value) => setFormData({ ...formData, nombreReceptor: value })}
                                mt={1}
                            />
                            {'nombreReceptor' in errors ?
                                <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.nombreReceptor}</FormControl.ErrorMessage>
                            : 
                                null
                            }                      
                        </FormControl>
                    : null
                    }

                    { modalOpenDireccionFormState.modalOpenDireccionForm.type == "Destino" ? 
                        <FormControl isInvalid={'telefonoReceptor' in errors}>
                            <FormControl.Label>Teléfono receptor</FormControl.Label>
                            <Input
                                placeholder="Ingrese teléfono receptor"
                                value={formData.telefonoReceptor}
                                onChangeText={(value) => setFormData({ ...formData, telefonoReceptor: value })}
                                mt={1}
                            />
                            {'telefonoReceptor' in errors ?
                                <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.telefonoReceptor}</FormControl.ErrorMessage>
                            : 
                                null
                            }                      
                        </FormControl>
                    : null
                    }

                    <FormControl isInvalid={'informacionAdicional' in errors}>
                        <FormControl.Label>Información adicional</FormControl.Label>
                        <Input
                            placeholder="Ingrese información adicional"
                            value={formData.informacionAdicional}
                            onChangeText={(value) => setFormData({ ...formData, informacionAdicional: value })}
                            mt={1}
                        />
                        {'informacionAdicional' in errors ?
                            <FormControl.ErrorMessage _text={{fontSize: 'xs', color: 'error.500', fontWeight: 500}}>{errors.informacionAdicional}</FormControl.ErrorMessage>
                        : 
                            null
                        }                      
                    </FormControl>
                </Stack>

                <Stack py={3}>
                    <Button variant="ghost" 
                        colorScheme={'gray'} 
                        onPress={() => {
                            onSubmit(formData)
                        }}                           
                        _text={{
                            fontWeight: "bold",
                            color: '#fc5404'
                        }}
                    >
                        Guardar
                    </Button>
                </Stack>
            </ScrollView>
        </Stack>
    );   
}


