import React from 'react';  
import { Center, View, Box, Text, Pressable, HStack, Button  } from 'native-base';  
import { Icon } from 'react-native-elements';

export class FooterComponent extends React.Component {  
  
    constructor(props) {  
        super(props);
        this.userDataState = props.userDataState;
        this.tabButtonFooterState = props.tabButtonFooterState;
        this.colorModeState = props.colorModeState;
    }  

    render() {
        const SearchPedidosButton = () => {
            return (
                <Pressable my={3} opacity={this.tabButtonFooterState.selectedTabButtonFooter === "Buscar" ? 1 : 0.5}
                    flex={1}
                    onPress={() => this.tabButtonFooterState.setSelectedTabButtonFooter("Buscar")}
                    >
                    <Center>
                        <Icon name='search'
                            type='font-awesome'
                            color='#fc5404'
                        />
                        <Text my={1} color={this.colorModeState.colorMode === 'light' ? 'gray.900' : 'gray.100'} bold>Buscar {this.userDataState.userData.typeUser == "Cadete" ? "Cliente" : "Cadete"}</Text>
                    </Center>
                </Pressable>
            );
        };

        const MisPedidosButton = () => {
            return (
                <Pressable my={3} opacity={this.tabButtonFooterState.selectedTabButtonFooter === "Mis Pedidos" ? 1 : 0.5}
                    flex={1}
                    onPress={() => this.tabButtonFooterState.setSelectedTabButtonFooter("Mis Pedidos")}
                    >
                    <Center>
                        <Icon name='list-alt'
                            type='font-awesome'
                            color='#fc5404'
                        />
                        <Text my={1} color={this.colorModeState.colorMode === 'light' ? 'gray.900' : 'gray.100'} bold>Mis Pedidos</Text>
                    </Center>
                </Pressable>
            );
        };

        const RankingButton = () => {
            // Si soy usuario GOLD
            if (this.userDataState.userData.typeAccount === "GOLD") {
                return (
                    <Pressable my={3} opacity={this.tabButtonFooterState.selectedTabButtonFooter === "Ranking" ? 1 : 0.5}
                        flex={1}
                        onPress={() => this.tabButtonFooterState.setSelectedTabButtonFooter("Ranking")}
                        >
                        <Center>
                            <Icon name='star'
                                type='font-awesome'
                                color='#fc5404'
                            />
                            <Text my={1} color={this.colorModeState.colorMode === 'light' ? 'gray.900' : 'gray.100'} bold>Ranking</Text>
                        </Center>
                    </Pressable>
                );
            } else {
                return null;
            }
        };

        return (
            <HStack bg="gray.100"  bg={this.colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}  alignItems="center" safeAreaBottom shadow={6}>
                <SearchPedidosButton/>
                <MisPedidosButton/>
                <RankingButton/>
            </HStack>
        );  
    }  
}  





