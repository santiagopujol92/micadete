import React, {useState} from 'react';  
import {
    FormControl,
    Select
} from 'native-base';  

export class SelectCadeteComponent extends React.Component {  

    constructor(props) {  
        super(props);
        this._isMounted = false;
        this.userDataState = props.userDataState;
        this.colorModeState = props.colorModeState;
        this.dataSelectCadeteState = props.dataSelectCadeteState;
        this.dataSelectCadeteSelectedState = props.dataSelectCadeteSelectedState;
    }  

    // component didmount se ejecuta antes de que se pinte, y use effect despues de q pinte (por lo tanto a veces hace parpadeos de imagen)
    componentDidMount(){
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render(){

        const onSelectCadete = (itemValue) => {
            this.dataSelectCadeteSelectedState.dataSelectCadeteSelected = itemValue;
            this.dataSelectCadeteSelectedState.setDataSelectCadeteSelected(this.dataSelectCadeteSelectedState.dataSelectCadeteSelected);
        }

        return(
            <FormControl isRequired >
                <FormControl.Label>Cadete</FormControl.Label>
                <Select
                    colorScheme={this.colorModeState.colorMode === 'dark' ? 'gray' : 'gray'}
                    backgroundColor={this.colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}
                    borderColor={this.colorModeState.colorMode === 'dark' ? 'gray.900' : 'gray.100'}

                    selectedValue={this.dataSelectCadeteSelectedState.dataSelectCadeteSelected}
                    minWidth={200}
                    accessibilityLabel="Seleccione uno de sus cadetes"
                    placeholder="Seleccione uno de sus cadetes"
                    onValueChange={(itemValue) => {
                        onSelectCadete(itemValue);
                    }}
                    placeholderTextColor={this.colorModeState.colorMode === 'dark' ? 'gray.500' : 'gray.500'}
                    mt={1}
                    fontWeight={"bold"}
                >
                    {this.dataSelectCadeteState.dataSelectCadete.map((item, index) => (
                        <Select.Item
                            label={item.name}  
                            value={item} 
                            key={item.id}
                            >
                        </Select.Item>
                    ))}

                </Select>
            </FormControl>
        )
    }
}  





