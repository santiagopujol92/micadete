import React from 'react';  
import {
    Text,
    Button,
    FlatList,
    Avatar,
    HStack,
    VStack,
} from 'native-base';  

export class ListMisPedidosComponent extends React.Component {  

    constructor(props) {  
        super(props);
        this.userDataState = props.userDataState;
        this.colorModeState = props.colorModeState;
        this.abmPedidoDataState = props.abmPedidoDataState;
        this.pantallaAnteriorABMPedidoState = props.pantallaAnteriorABMPedidoState;
        this.misPedidosDataState = props.misPedidosDataState;
        this._isMounted = false;
    }  

    // component didmount se ejecuta antes de que se pinte, y use effect despues de q pinte (por lo tanto a veces hace parpadeos de imagen)
    componentDidMount(){
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render(){

        const ItemParaCliente  = ({ item }) => (
            <HStack space={3} mb={0} 
                    bg={this.colorModeState.colorMode === 'dark' ? 'gray.800' : 'gray.200'} >
                <Button variant="ghost" borderRadius={0} py={0.5}
                        onPress={() => openABMPedido({item})}
                        colorScheme={this.colorModeState.colorMode === 'light' ? 'orange' : 'gray'} 
                    >
                    <HStack >
                        <Avatar my={3} size={12} source={{
                                    uri: item.userCliente.picture.data.url 
                                }}
                            >
                            <Avatar.Badge bg={"red.200"} />
                        </Avatar>
                        <VStack mx={4} space={0} w="80%"
                                p={1}
                                borderRadius={5}
                            > 
                            <Text bold>Cadete {item.userCadete.name} - {item.estado}</Text>
                            <Text fontSize={14} color={this.colorModeState.colorMode === 'dark' ? 'gray.400' : 'gray.600'}>
                                Retirar: {item.horaRetiroDesde} - {item.horaRetiroHasta} - Entrega: {item.fechaEntrega}
                            </Text>
                            <Text fontSize={14} color={this.colorModeState.colorMode === 'dark' ? 'gray.400' : 'gray.600'}>
                                Direccion: {item.direccionDestino.calle} {item.direccionDestino.numero} 
                            </Text>
                        </VStack>
                    </HStack>
                </Button>
            </HStack>
        );
    
        const ItemParaCadete = ({ item }) => (
            <HStack space={3} mb={0} 
                    bg={this.colorModeState.colorMode === 'dark' ? 'gray.800' : 'gray.200'} >
                <Button variant="ghost" borderRadius={0} py={0.5}
                        onPress={() => openABMPedido({item})}
                        colorScheme={this.colorModeState.colorMode === 'light' ? 'orange' : 'gray'} 
                    >
                    <HStack >
                        <Avatar my={3} size={12} source={{
                                    uri: item.userCliente.picture.data.url 
                                }}
                            >
                            <Avatar.Badge bg={"red.200"} />
                        </Avatar>
                        <VStack mx={4} space={0} w="80%"
                                p={1}
                                borderRadius={5}
                            > 
                            <Text bold>Cliente {item.userCliente.name} - {item.estado}</Text>
                            <Text italic color={this.colorModeState.colorMode === 'dark' ? 'gray.400' : 'gray.600'}>
                                Retirar: {item.horaRetiroDesde} - {item.horaRetiroHasta} - Entrega: {item.fechaEntrega}
                            </Text>
                            <Text italic color={this.colorModeState.colorMode === 'dark' ? 'gray.400' : 'gray.600'}>
                                Direccion: {item.direccionDestino.calle} {item.direccionDestino.numero} 
                            </Text>
                        </VStack>
                    </HStack>
                </Button>
            </HStack>
        );

        const RenderItem = ({ item }) => {
            // Si tiene item
            if (item) {
                // Si el tipo de usuario
                switch (this.userDataState.userData.typeUser) {
                    case ("Cliente"):
                        // Si el pedido tiene seteado un cliente y es igual a la del logeado
                        return item.userCliente != null && item.userCliente.id === this.userDataState.userData.id ? ItemParaCliente({item}) : null;
                    case ("Cadete"):
                        // Si el pedido tiene seteado un cadete y es igual a la del logeado
                        return item.userCadete != null && item.userCadete.id === this.userDataState.userData.id ? ItemParaCadete({item}) : null;
                    default: return null;
                }
            } else {
                return (
                    <Text>No tiene pedidos asignados</Text>
                );
            }
        };

        const openABMPedido = ({ item }) => {
            if (item) {
                this.abmPedidoDataState.setABMPedidoData(item);
                this.pantallaAnteriorABMPedidoState.setPantallaAnteriorABMPedido("Mis Pedidos");
            }
        }

        return(
             <FlatList style={{width:'100%'}}
                data={this.misPedidosDataState.misPedidosData}
                keyExtractor={(item)=>item.key}
                renderItem={({item})=>{
                    return <RenderItem
                        item={item} 
                    /> 
                }}
            />
        )
    }
}  





