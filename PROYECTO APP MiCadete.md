# PROYECTO APP MiCadete #

### INTRODUCCION ###
    App de cadeteria para cadetes y clientes
    Free para pedidos
    version Gold para obtener ventajas en pedidos y estadisticas generales para CLIENTE y cadetes

### TECNOLOGIAS ###

    * REACT NATIVE JS
    * EXPO CLI
    * NATIVE BASE TEMPLATE
    * FIREBASE CLOUD (SOUTH AMERICA SERVER)

### COMPONENTES DESTACADOS ###

    * FACEBOOK LOGIN (ANDROID, IOS)
    * GOOGLE MAPS
    * NOTIFICACIONES PUSH
    * NOTIFICACIONES EMAIL
    * MERCADO PAGO (DEPOSITO A MI CUENTA O MI VIEJO, PARA CUENTAS GOLD, SE DEBE ACTUALIZAR MENSUALMENTE)

### RECURSOS DE DISPOSITIVO NECESARIOS ### 
    * UBICACION (PARA SEGUIMIENTO DE ENVIOS POR MAPA)
    * CAMARA (PARA ADJUNTAR UNA FOTO EN RECLAMO DE PEDIDOS)

### PERMISOS DE ACCESO EN PANTALLAS ### 
https://docs.google.com/document/d/1oIIz2mKVq9W0jhlIY6GbV8IdMtBcsbdgmMb54xHMXrM/edit#

    * USUARIO NIVEL FREE
        - TIPO CLIENTE:
            * "CREAR PEDIDO":
                ACCESO, VER CADETES (puntaje promedio, ultimo puntaje de usuario, ultimo puntaje de pedido, estado del motociclista(disponible, no disponible, vuelvo enseguida), disponibilidad de cadete, ver perfil (visualizar los puntajes del usuario), hacer 3 pedido (poniendo direccion origen y destino) por dia, recomendar, no recomendar)

            * "MI PERFIL": 
                (TIPO DE USUARIO Y NIVEL, PEDIDOS REALIZADOS, ACEPTADOS, PUNTAJE PROMEDIO)
                CON DATOS DE FACE Y POSIBILIDAD DE CAMBIARSE DE TIPO DE CUENTA (una vez por mes), TOTAL SI ASIGNO UN PEDIDO NO PUEDO CREARSELO ASI MISMO PORQUE NO ERA CADETE EN ESE MOMENTO, ENTONCES NUNCA SE JUNTAN LOS MOMENTOS

            * "MIS PEDIDOS":
                ACCEDER Y SEGUIR EL PEDIDO ACTUAL, RECHAZARLO, RECLAMAR

            * "MIS RECLAMOS":
                RECLAMOS DE PEDIDOS REALIZADOS Y POSIBILIDAD DE SOLUCIONARLO POR WSP. Y VER QUE OPERACION HACER

        - TIPO CADETE:
            * "PEDIDOS": 
                POSIBILIDAD DE FILTRAR POR LOCALIDAD Y PROVINCIA, POR NOMBRE DE PRODUCTO, 
                TOMAR 10 O 20 (despues se ve el limite) PEDIDOS POR DIA, SI LE ASIGNARON UN PEDIDO Y LE APARECE PRIMERO, POSIBILIDAD DE RECHAZARLO O ACEPTARLO,
                VER PERFIL DEL USUARIO QUE REALIZO EL PEDIDO

            * "MI PERFIL" 
                (CON DATOS DE FACE Y POSIBILIDAD DE CAMBIARSE DE TIPO DE CUENTA (una vez por mes), TOTAL SI ASIGNO UN PEDIDO NO PUEDO CREARSELO ASI MISMO PORQUE NO ERA CADETE EN ESE MOMENTO, ENTONCES NUNCA SE JUNTAN LOS MOMENTOS)

            * "RECLAMOS":
                RECLAMOS DE PEDIDOS HECHO HACIA MI Y POSIBILIDAD DE SOLUCIONARLO POR WSP. Y VER QUE OPERACION HACER

    * USUARIO NIVEL GOLD SE AGREGA: 
        - TIPO CLIENTE
            - POSIBILIDAD DE REALIZAR MAS DE 3 PEDIDOS DIARIO (limite variable por db, se debe obtener al logearse y al realizar un pedido se debe verificar previamente)
            - ACCESO GENERAL A ESTADISTICAS CON MIS CADETES

        - TIPO CADETE:
            - POSIBILIDAD DE TOMAR MAS DE 10 PEDIDOS DIARIOS (limite variable por db, se debe obtener al logearse y al tomar un pedido se debe verificar previamente)
            - ACCESO GENERAL A ESTADISTICAS CON MIS CLIENTES
            - ACCESO GENERAL A COTIZADOR DE ENVIO

### NOTIFICACIONES PUSH Y EMAIL ###

     UN RECLAMO DEL PEDIDO DEL CLIENTE SIENDO CADETE
     UNA OBSERVACION COMO USUARIO SIENDO CADETE O CLIENTE
     CUANDO TE HACEN UN PEDIDO A TU NOMBRE SIENDO CADETE

### ESTRUCTURA DE DATOS CLOUD FIREBASE DB ### 
    TABLAS:
        * USUARIOS(facebook key, tipo (cliente,cadete), nivel (free, gold))
        * PEDIDOS (antes de crearse se verifica q nivel de usuario es y si es free y ya supero el limite chau. || Estado, ClienteUserId, CadeteUserId, FechaABM, infoAdicionalCadete, costoPedido, cordenadasDestinoX, cordenadasDestinoY, Calle Destino, Numero, Piso, EntreCalles, LocalidadText, ProvinciaText, UrgenciaHoraria)
        * RECLAMOS DE PEDIDO (observacion, imagen, etc))
        * OBSERVACIONES DE USUARIO (puntaje (1 al 5), observacion)
        a terminar de pensar
        * LIMITES DE PEDIDOS NIVELES(limitePedirFree, limitePedirGold, limiteTomarFree, limiteTomarGold)

### INFORMACIÓN ADICIONAL ### 
    -