import React from 'react';
import { IndexView } from './src/views/IndexView';
import { 
    NativeBaseProvider, 
    extendTheme
} 
from 'native-base';

const config = {  
    useSystemColorMode: true, 
    initialColorMode: 'dark' 
};

const customTheme = extendTheme({ config });

export default function App() {
    return (
        <NativeBaseProvider theme={customTheme}>
            <IndexView />
        </NativeBaseProvider>
    );
}
